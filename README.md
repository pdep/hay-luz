# Hay Luz PdeP

## Miercoles Noche

Para poder levantar el proyect se necesita [create-elm-app](https://github.com/halfzebra/create-elm-app)

### Desarollo

 1. Tener instalado Node >= 8 (se puede verificar con `node -v` o instalar desde [la página de Node](https://nodejs.org/es/download/)
 2. Instalar `create-elm-app` (ejecutar `npm install create-elm-app -g`)
 3. Levantar el servidor ejecutando `elm-app start`
