module Cosas exposing (Informacion, Posicion(..), clickDecoder, const, siemprePrevenir, x, y)

import Json.Decode as Decode


type Posicion
    = Posicion Float Float


x (Posicion coordenada _) =
    coordenada


y (Posicion _ coordenada) =
    coordenada


type alias Informacion =
    ( Posicion, Bool )


clickDecoder : (Posicion -> msg) -> Decode.Decoder msg
clickDecoder f =
    Decode.map f <|
        Decode.map2 Posicion
            (Decode.field "offsetY" Decode.float)
            (Decode.field "offsetX" Decode.float)


const : a -> b -> a
const constante _ =
    constante


siemprePrevenir : a -> ( a, Bool )
siemprePrevenir msg =
    ( msg, True )


flip a b c =
    a c b
