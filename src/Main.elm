module Main exposing (Modelo, actualizar, init, main, vista)

import Browser
import Cosas exposing (Informacion, Posicion(..), clickDecoder, const, siemprePrevenir, x, y)
import Html exposing (Html, button, div, h1, span, text)
import Html.Attributes exposing (class, src, style)
import Html.Events exposing (on, onClick, preventDefaultOn)
import Json.Decode as Decode
import List exposing (filter, length, map)
import Random



---- MODEL ----


type alias Modelo =
    { hayLuz : List Posicion
    , noHayLuz : List Posicion
    }


initModelo : Modelo
initModelo =
    { hayLuz = [], noHayLuz = [] }


init : ( Modelo, Cmd Msg )
init =
    ( initModelo, Cmd.none )



---- ACTUALIZAR ----


type alias Msg =
    Modelo -> Modelo


actualizar : Msg -> Modelo -> ( Modelo, Cmd Msg )
actualizar msg model =
    ( msg model, Cmd.none )


resetear : Msg
resetear modelo =
    initModelo


cuandoHaceClick : Posicion -> Msg
cuandoHaceClick posicion =
    \modelo ->
        { modelo
            | hayLuz =
                posicion :: modelo.hayLuz
        }


cuandoHaceElOtroClick : Posicion -> Msg
cuandoHaceElOtroClick posicion =
    \modelo ->
        { modelo
            | noHayLuz =
                posicion :: modelo.noHayLuz
        }



---- VISTA ----


cuantosSi : Modelo -> String
cuantosSi modelo =
    modelo.hayLuz
        |> length
        |> String.fromInt


cuantosNo : Modelo -> String
cuantosNo modelo =
    "0"


vista : Modelo -> Html Msg
vista modelo =
    div []
        [ h1 [] [ text "Hay luz?" ]
        , div []
            [ div []
                [ span [] [ text "Si: " ]
                , span [] [ text <| cuantosSi modelo ]
                ]
            , div []
                [ span [] [ text "Nop: " ]
                , span [] [ text <| cuantosNo modelo ]
                ]
            ]
        , div []
            [ button [ onClick resetear ] [ text "Reset" ]
            ]
        , div [ class "mapa" ]
            [ div
                [ class "container"
                , on "click" <| clickDecoder cuandoHaceClick
                , preventDefaultOn "contextmenu" <| Decode.map siemprePrevenir <| clickDecoder cuandoHaceElOtroClick
                ]
              <|
                (map puntitoConLuz <|
                    .hayLuz modelo
                )
                    ++ (map puntitoSinLuz <|
                            .noHayLuz modelo
                       )
            ]
        ]


puntitoSinLuz : Posicion -> Html Msg
puntitoSinLuz posicion =
    div
        [ class "puntito"
        , class "noHay"
        , style "top" <| String.fromFloat (x posicion) ++ "px"
        , style "left" <| String.fromFloat (y posicion) ++ "px"
        ]
        [ text " " ]


puntitoConLuz : Posicion -> Html Msg
puntitoConLuz posicion =
    div
        [ class "puntito"
        , class "hay"
        , style "top" <| String.fromFloat (x posicion) ++ "px"
        , style "left" <| String.fromFloat (y posicion) ++ "px"
        ]
        [ text " " ]


hayClass : Bool -> String
hayClass b =
    case b of
        True ->
            "hay"

        False ->
            "noHay"



---- PROGRAM ----


main : Program () Modelo Msg
main =
    Browser.element
        { view = vista
        , init = \_ -> init
        , update = actualizar
        , subscriptions = always Sub.none
        }
